use rug::rand::RandState;
use rug::Integer;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 3 {
        println!("{} <digs of p and q> <# of problems>", args[0]);
        return;
    }

    let mut rng = RandState::new();
    let digs = args[1].parse().unwrap();
    let num_problems = args[2].parse().unwrap();
    for _ in 0..num_problems {
        generate_problem(digs, &mut rng);
    }
}

fn generate_problem(digs: u32, rng: &mut RandState) {
    let p = generate_prime(digs, rng);
    let q = generate_prime(digs, rng);
    let n = Integer::from(&p * &q);
    println!("{} {} {}", n, p, q);
}

fn generate_prime(digs: u32, rng: &mut RandState) -> Integer {
    let r = Integer::from(Integer::random_bits(digs * 3, rng));
    r.next_prime()
}
